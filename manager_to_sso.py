import pyodbc
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import json
import sys
import logging
import time
import datetime


def get_token(school_id, application, xgh_mode):

    if xgh_mode == True and application == 'financeiro':
        if school_id in [1	,
            2	,
            3	,
            5	,
            6	,
            7	,
            8	,
            9	,
            10	,
            11	,
            12	,
            13	,
            14	,
            16	,
            17	,
            18	,
            19	,
            20	,
            21	,
            22	,
            23	,
            24	,
            25	,
            26	,
            28	,
            29	,
            30	,
            31	,
            32	,
            33	,
            34	,
            35	,
            36	,
            37	,
            38	,
            39	,
            40	,
            41	,
            42	,
            43	,
            44	,
            45	,
            46	,
            47	,
            48	,
            49	,
            50	,
            51	,
            52	,
            53	,
            54	,
            55	,
            58	,
            59	,
            60	,
            61	,
            62	,
            63	,
            64	,
            65	,
            66	,
            67	,
            68	,
            69	,
            70	,
            71	,
            72	,
            73	,
            75	,
            77	,
            80	,
            81	,
            83	,
            84	,
            85	,
            86	,
            88	,
            94	,
            95	]:
            return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRfaWQiOiJlMjRiNzdhYS1kYTgyZTU0Mjc0NzcifQ.yH8bF_cXgttk3A7Qa11zyio4mc5TGj1_rDgu_P9ffI0"
        elif school_id in [89,90,91,92,93]:
            return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRfaWQiOiJlNjcxZmQ5Ni04MmEzNDFjYTM3YzcifQ.aRTrivFC3Qr-7mc6dzt9nizPwNwdzKv2h2cza-bX9mQ"
        elif school_id in [15,74,82]:
            return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRfaWQiOiJhYjE4ZTE5NC0yNDNkOTE0MTcyOWYifQ.10k6OqtE0KlGDzh20L4ZXY8tQH0rzJLdqpEdJJ0cRcw"
        elif school_id in [4,27]:
            return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRfaWQiOiI5YzM5MTViNi1mYmQ1ZTc3MGVmYWUifQ.3rvsNWGikFSpvDmLLgXHiupezpXi0oP1dWfHi_JaPIc"
        else: 
            return None
    if xgh_mode == True and application == 'sebdigital':
        if school_id in [1, 2,  3, 24, 37, 39, 40,  44, 46,  47,  48,  49, 52,  53, 54, 16, 17, 18, 19, 20, 37, 38,39,40,41]: 
            return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRfaWQiOiJkNTgyYzA3My02YjQyMzU5MWFiZTcifQ.d6tZONVb0Vqny2HpTgbp6yqDE-NFNi84zziQcwR9jbg"
        elif school_id in [11,12,13,14,51]:
            return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRfaWQiOiJiYjYxZTU3OS1lZGQ0N2E3MTk5ZGQifQ.Dxa2kyZmHRcIyR_aezm2THavBzG8wOEIgk98ZZg8Nso"
        elif school_id in [4]:
            return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRfaWQiOiI3YmI2YTNmZC01ZWViNTkwZGIyZmEifQ.LTGMHcYUp_fEYFiOtsCE_XthkLkijXuTSW7zr4yfipU"
        else: 
            return None

    # token = tokens.get(school_id, None)
    # if token == None:
    #     with open('parameters.json','r') as pr:
    #         jsonfile = json.load(pr)
    #         for n in jsonfile:
    #             if n["name_key"]==application:
    #                 for c in n['credentials']:
    #                     if school_id in [int(e) for e in c['school_id'].split(',')]:
    #                         token[school_id] = c['token']
    #                         ###todo: ugly??
    #                         return c['token']
    # return token


def getdata(conn):
    lines = []
    
    try:
        cursor = conn.cursor()
        cursor.execute('EXEC [dbo].[sp_export_data_to_sso]') 
        row = cursor.fetchone() 
        while row: 
            param = build_params(row)
            lines.append(param)
            row = cursor.fetchone()
        del cursor
    except pyodbc.Error as ex:
            print(ex)
        
    return lines
        

def build_params(row):
    aditional_data = {}
    aditional_data['PES_Codigo'] = row.pes_codigo
    aditional_data['USU_Codigo'] = row.usu_codigo
    aditional_data['ACS_1'] = row.ACS_1
    aditional_data['ACS_2'] = row.ACS_2
    params = {}
    params["school_id"] = row.UNI_Codigo
    params["name"] = row.NAME
    if row.email:
        params["email"] = row.email
    params["legal_number"] = row.cpf
    params["type"] = row.type
    params["phone"] = row.phone
    #   data["profile_picture_url"] = row.profile_picture_url
    params["password"] = row.password
    params["password_repeat"] = row.password_repeat
    params["birth"] = row.birth
    params["cep"] = row.cep
    params["genre"] = row.genre
    params['additional_data'] = aditional_data
    return params

def call_sso(token,params):
    endpoint = ''.join([baseurl,apiversion,'user'])
    httpheader = {'Authorization':'%s' % (token), 'Content-type': 'application/json', 'Accept': 'text/plain','user-agent': 'Integração Manager/1.0.0'}
    del params["school_id"]
    t0 = time.time()
    try:
        response = requests_retry_session().post(endpoint, json=params, headers=httpheader)
        if response.ok:
            return { 'ok':True, 'message': response.content }
        else:
            return { 'ok':False, 'message': response.content }
    except Exception as x:
        logging.error("Error calling %s. Error %s", endpoint,  x.__class__.__name__)
    else:
        logging.info("Eventually worked on calling %s. Status Code: %s", endpoint, response.status_code)
    finally:
        t1 = time.time()
        logging.info("Processed in %s ms ", t1-t0)


def do_log(person_id, success, message):
    if success == True:
        logging.info('%s - successfull integrated. Message is: %s' % (person_id, message))
    else:
        logging.error('Error on try to integrate %s: %s' % (person_id, message))


def update_sso_status(conn, status, id ):
    cursor = conn.cursor()
    cursor.execute('update GER_Pessoa set PES_STATUS_SSO = ? where pes_codigo = ?',status, id) 
    cursor.commit()
    del cursor
 
    

def requests_retry_session(
    retries=3,
    backoff_factor=0.3,
    status_forcelist=(500, 502, 504),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('https://',adapter)
    session.mount('http://',adapter)
    return session

tokens = {}

app = sys.argv[1]
ambient = sys.argv[2] 
#params
baseurl = "https://apptisebsso-api-apptisebsso-api-homolg.azurewebsites.net/api/"
apiversion = 'v2/'
server = "10.0.218.44"
username = "UserGerenciador50"
password = "UserGerenciador50#2016"
database = "manager50_dev"

print(ambient)

if ambient == 'prd':
    baseurl = "https://sso-apis.sebsa.com.br/api/" 
    server = "10.0.210.60"
    username = "UserGerenciador50"
    password = "UserGerenciador50#SEB.2015"
    database = "manager50"

        

logging.basicConfig(level = logging.INFO, filename = time.strftime("my-%Y-%m-%d.log"),format = '%(asctime)s  %(levelname)-10s %(processName)s  %(name)s %(message)s')
logging.info("Integração iniciada em %s" %(datetime.datetime.now()))
#Connect to database
connection = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)

# starttime = time.time()
# while True:
data = getdata(connection)
for row in data:
    school_token = get_token(row['school_id'],app, True)
    if school_token != None: 
        success_obj = call_sso(school_token,row)
    if success_obj.get('ok') == True:
        do_log(row["additional_data"]['PES_Codigo'], True, success_obj.get('message'))
        update_sso_status(connection,"SUCCESS",row["additional_data"]['PES_Codigo'])
    else:
        do_log(row["additional_data"]['PES_Codigo'], False, success_obj.get('message'))
        update_sso_status(connection,"ERROR",row["additional_data"]['PES_Codigo'])
# time.sleep(60.0 - ((time.time() - starttime) % 60.0))
connection.close()

logging.info("Integração finalizada em %s" %(datetime.datetime.now()))
        

  